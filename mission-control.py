#!/bin/python3

import os
import sys
from datetime import datetime
import json

satellites = {}

# process business rules
def process(item):
    if item["component"] == "TSTAT" and item["raw-value"] > item["red-high-limit"]:
        if satellites[item["satellite-id"]][item["component"]]["count"] == 0:
            satellites[item["satellite-id"]][item["component"]]["value"]= item

        satellites[item["satellite-id"]][item["component"]]["count"] += 1
                
    elif item["component"] == "BATT" and item["raw-value"] < item["red-low-limit"]:
        if satellites[item["satellite-id"]][item["component"]]["count"] == 0:
            satellites[item["satellite-id"]][item["component"]]["value"]= item

        satellites[item["satellite-id"]][item["component"]]["count"] += 1

if __name__ == '__main__':
    data = sys.stdin.readlines()


    # build the telemetry data
    td = (list(map(lambda x: x.rstrip().split('|'), data)))

    telemetry_data = list(map(lambda x: { "date" : datetime.strptime(x[0], '%Y%m%d %H:%M:%S.%f'), 
                        "satellite-id" : int(x[1]), 
                        "red-high-limit" : float(x[2]), 
                        "yellow-high-limit" : float(x[3]), 
                        "yellow-low-limit" : float(x[4]), 
                        "red-low-limit" : float(x[5]),
                        "raw-value" : float(x[6]),
                        "component" : x[7]},
            td))


    alerts = []

    for item in telemetry_data:
        # initialize dictionary
        if item["satellite-id"] not in satellites:
            satellites[item["satellite-id"]] = {} 
            
        if item["component"] not in satellites[item["satellite-id"]]:
            satellites[item["satellite-id"]][item["component"]] = { 
                                                                    "value" : {"date" : item["date"]},
                                                                    "count" : 0
                                                                    }

        # calculate and check for the duration
            
        duration = satellites[item["satellite-id"]][item["component"]]["value"]["date"] - item["date"]

        # print(item["satellite-id"], item["component"], 
        #     item["date"],
        #     duration.total_seconds(), 
        #     item["raw-value"], 
        #     item["red-high-limit"],
        #     item["raw-value"] > item["red-high-limit"],
        #     item["red-low-limit"],          
        #     item["raw-value"] < item["red-low-limit"])   
        
        if duration.total_seconds() < -300:
            satellites[item["satellite-id"]][item["component"]] = { 
                                                                    "value" : {
                                                                                "date" : item["date"]
                                                                            },
                                                                    "count" : 0
                                                                }
            process(item)        
        else:
            process(item)
                    
        if satellites[item["satellite-id"]][item["component"]]["count"] == 3:
            severity = "RED HIGH"
            if item["component"] == "BATT":
                severity = "RED LOW"
                
            d = satellites[item["satellite-id"]][item["component"]]["value"]["date"]
                
            alerts.append({
                "satelliteId" : item["satellite-id"],
                "severity": severity,
                "component": item["component"],
                "timestamp": datetime.strftime(d, '%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
            })

    # for item in satellites:
    #     for c in satellites[item]:
    #         print(item, c, satellites[item][c]["value"]["date"], satellites[item][c]["count"])

    print(json.dumps(alerts, indent=4))